<?php
/*
*	Marcin Michałek
*	cli-config.php
* 	01.09.2015
*/
require_once "startDb.php";

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);