<?php
/*
*	Marcin Michałek
*	listOfWorkers.php
* 	01.09.2015
*/

include_once('startDb.php');
include_once('model/worker.php');
include_once('configuration.php');

$workersRepository = $entityManager->getRepository('Worker');
$workers = $workersRepository->findAll();


?>

<div class="row">
	<div class="col-md-6 col-md-offset-3 basic panel panel-default">
		<br>
			<a class="btn btn-primary btn-sm" href="addNewEmployee.php"> Add new employee </a>
		<br>
		<br>		
		<div class="panel panel-default">
		<table class="table">
			<tr>
				<td>
				</td>
				<td>
					<b>Firstname</b>
				</td>
				<td>
					<b>Lastname</b>
				</td>
				<td>
					<b>Workplace</b>
				</td>	
				<td>			
				</td>
			</tr>
			<?php
			foreach ($workers as $worker) {
			?>	
				<tr>
					<td><img src="<?php echo $smallImgPath.$worker->getImgSrc().'.jpeg'; ?>" alt="Employee photo" height="40" width="40"></td>
					<td><?php echo $worker->getFirstName(); ?></td>
					<td><?php echo $worker->getLastName(); ?></td>
					<td><?php echo $worker->getWorkplace(); ?></td>
					<td><a class="btn btn-primary btn-sm" href="detailsOfEmployee.php?id=<?php echo $worker->getId(); ?>">Details</a></td>
				</tr>
			<?php	
			}
			?>
		</table>
		</div>
	</div>
</div>


<!--
<div class="row">
	<div class="col-md-4 basic panel panel-default">
		<h2> Cokolwiek </h2>
	</div>

	<div class="col-md-8 basic panel panel-default" id="abc">  

	</div>
</div>
-->