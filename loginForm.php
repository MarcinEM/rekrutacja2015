<?php
/*
*	Marcin Michałek
*	loginForm.php
* 	01.09.2015
*/
?>

<div class="row">
	<div class="col-md-6 col-md-offset-3 basic panel panel-default">
		<h2><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span><br> Login</h2>
		<br>
		<?php
			if(isset($_POST['username']) && isset($_POST['password'])){		
				echo '<div class="alert alert-danger" role="alert">Incorect username or password!</div>';
			}
		?>
		<form role="form" action="login.php" method="post">
			<label>Username</label>		
			<br>
			<input id="username" name="username" autofocus>
			<br>
			<label>Password</label>
			<br>
			<input id="password" name="password" type="password" value="">				
			<br><br>						
			<button class="btn btn-primary btn-sm" type="submit"> Login </button>	
			<br>
		</form>
		<br><br>
	</div>
</div>		