<?php
/*
*	Marcin Michałek
*	worker.php
* 	01.09.2015
*/

use Doctrine\ORM\Mapping as ORM;

/**
 * @Entity @Table(name="workers")
 **/
class Worker
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="string") **/
    protected $firstName;
    /** @Column(type="string") **/
    protected $lastName;
    /** @Column(type="date") **/
    protected $dayOfEmployment;
    /** @Column(type="date") **/
    protected $dayOfBirth;	
    /** @Column(type="string") **/
    protected $workPlace;	
    /** @Column(type="string") **/
    protected $education;
    /** @Column(type="string") **/
    protected $phoneNumber;	
    /** @Column(type="string") **/
    protected $adress;
    /** @Column(type="integer") **/
    protected $salary;
    /** @Column(type="integer") **/
    protected $bonus;	
    /** @Column(type="string") **/
    protected $imgSrc;	


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return worker
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return worker
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set dayOfEmployment
     *
     * @param \DateTime $dayOfEmployment
     *
     * @return worker
     */
    public function setDayOfEmployment($dayOfEmployment)
    {
        $this->dayOfEmployment = $dayOfEmployment;

        return $this;
    }

    /**
     * Get dayOfEmployment
     *
     * @return \DateTime
     */
    public function getDayOfEmployment()
    {
        return $this->dayOfEmployment;
    }

    /**
     * Set dayOfBirth
     *
     * @param \DateTime $dayOfBirth
     *
     * @return worker
     */
    public function setDayOfBirth($dayOfBirth)
    {
        $this->dayOfBirth = $dayOfBirth;

        return $this;
    }

    /**
     * Get dayOfBirth
     *
     * @return \DateTime
     */
    public function getDayOfBirth()
    {
        return $this->dayOfBirth;
    }

    /**
     * Set workPlace
     *
     * @param string $workPlace
     *
     * @return worker
     */
    public function setWorkPlace($workPlace)
    {
        $this->workPlace = $workPlace;

        return $this;
    }

    /**
     * Get workPlace
     *
     * @return string
     */
    public function getWorkPlace()
    {
        return $this->workPlace;
    }

    /**
     * Set education
     *
     * @param string $education
     *
     * @return worker
     */
    public function setEducation($education)
    {
        $this->education = $education;

        return $this;
    }

    /**
     * Get education
     *
     * @return string
     */
    public function getEducation()
    {
        return $this->education;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return worker
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set adress
     *
     * @param string $adress
     *
     * @return worker
     */
    public function setAdress($adress)
    {
        $this->adress = $adress;

        return $this;
    }

    /**
     * Get adress
     *
     * @return string
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * Set salary
     *
     * @param integer $salary
     *
     * @return worker
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;

        return $this;
    }

    /**
     * Get salary
     *
     * @return integer
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * Set bonus
     *
     * @param integer $bonus
     *
     * @return worker
     */
    public function setBonus($bonus)
    {
        $this->bonus = $bonus;

        return $this;
    }

    /**
     * Get bonus
     *
     * @return integer
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * Set imgSrc
     *
     * @param string $imgSrc
     *
     * @return worker
     */
    public function setImgSrc($imgSrc)
    {
        $this->imgSrc = $imgSrc;

        return $this;
    }

    /**
     * Get imgSrc
     *
     * @return string
     */
    public function getImgSrc()
    {
        return $this->imgSrc;
    }
}
