<?php
/*
*	Marcin Michałek
*	login.php
* 	01.09.2015
*/
include_once('startDb.php');
include_once('model/user.php');
include_once('configuration.php');

$loginFlag = false;

if(session_status() == PHP_SESSION_NONE){
	session_start();
}

if(isset($_POST['username']) && isset($_POST['password'])){	
	$userRepository = $entityManager->getRepository('User');
	$user = $userRepository->findBy(array('username' => $_POST['username']));
	if($user != NULL){
		if($user[0]->getPassword() == hash('sha256', $_POST['password'].$SALT)){
			$_SESSION['login'] = 'yes';
			$loginFlag = true;
		}			
	}
}

if(!isset($_SESSION['login'])){
	$_SESSION['login'] = 'no';
	include_once('header.php');
	include_once('loginForm.php');	
	include_once('footer.php');
}elseif($_SESSION['login'] != 'yes'){
	include_once('header.php');
	include_once('loginForm.php');
	include_once('footer.php');	
}

if($loginFlag === true){
	include_once('header.php');
	include_once('listOfWorkers.php');	
	include_once('footer.php');	
}



?>