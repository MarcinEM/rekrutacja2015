<?php
/*
*	Marcin Michałek
*	header.php
* 	01.09.2015
*/
?>
<head>
<META HTTP-EQUIV="content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- bootstrap.min.css -->
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<!-- JavaScript -->
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Moje css -->
<link rel="stylesheet" href="css/main.css">
</head>
<body>

<br><br><br><br>
<div class="row">
	<a href="index.php" class="col-md-6 col-md-offset-3 basic panel panel-default">
		<div class="row">
			<?php
				if($_SESSION['login'] == 'yes'){
					echo '<table class="col-md-10 col-md-offset-1">';	
				}else{
					echo '<table class="col-md-8 col-md-offset-2">';	
				}
			?>
			<!--
			<table class="col-md-8 col-md-offset-2">]
			-->
				<td>
					<img src="imgOfView/logo.gif"/>
				</td>
				<td>
					<h2> Workers Manager </h2>
				</td>
				<?php
					if($_SESSION['login'] == 'yes'){
						echo '<td><h3><a href="logout.php"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>Logout</a></h3></td>';
					}
				?>
			</table>			
		</div>
	</a>
</div>