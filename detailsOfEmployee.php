<?php
/*
*	Marcin Michałek
*	detailsOfEmployee.php
* 	03.09.2015
*/

include_once('login.php');
if(isset($_SESSION['login'])){
	if($_SESSION['login'] != 'yes'){
		die();
	}
}else{
	die();
}
include_once('startDb.php');
include_once('model/worker.php');
include_once('configuration.php');
include_once('header.php');


if(!isset($_GET['id'])){	
	$flag = false;
}else{
	$workerRepository = $entityManager->getRepository('Worker');
	$worker = $workerRepository->find($_GET['id']);
	if($worker === NULL){
		$flag = false;
	}else{
		$flag = true;
	}
}


	
if($flag === true){
?>
	<div class="row">
		<div class="col-md-6 col-md-offset-3 basic panel panel-default row">
			<br><br>
			<div class="col-md-8 col-md-offset-2">
				<table class="table">
					<tr>
						<td colspan="2" class="basic">
							<img src="<?php echo $bigImgPath.$worker->getImgSrc().'.jpg'; ?>" alt="Employee photo" height="200" width="200">
						</td>						
					</tr>
					<tr>
						<td colspan="2" class="basic">
					<h4><?php echo $worker->getFirstname().' '.$worker->getLastname(); ?></h4>
						</td>
					</tr>
					<tr>
						<td>
					Day of birth: 
						</td>
						<td>
					<?php echo $worker->getDayOfBirth()->format('d-m-Y'); ?>
						</td>
					</tr>
					<tr>
						<td>
					Workplace: 
						</td>
						<td>
					<?php echo $worker->getWorkplace(); ?>
						</td>
					</tr>
					<tr>
						<td>
					Date of employment: 
						</td>
						<td>
					<?php echo $worker->getDayOfEmployment()->format('d-m-Y'); ?>
						</td>
					</tr>
					<tr>
						<td>
					Education:
						</td>
						<td>
					<?php echo $worker->getEducation(); ?>
						</td>				
					</tr>
					<tr>
						<td>
					Phone number: 
						</td>
						<td>
					<?php echo $worker->getPhoneNumber(); ?>
						</td>
					</tr>
					<tr>
						<td>
					Adress:
						</td>
						<td>
					<?php echo $worker->getAdress(); ?>	
						</td>
					</tr>
					<tr>
						<td>
					Salary:
						</td>
						<td>
					<?php echo $worker->getSalary(); ?>	
						</td>
					</tr>
					<tr>
						<td>
					Bonus:
						</td>
						<td>
					<?php echo $worker->getBonus(); ?>
						</td>
					</tr>
				</table>
			</div>
			<a class="btn btn-primary btn-sm" href="index.php"> Go to employees list </a>
			<br>
			<br>
			<br>
	</div>
<?php 
}else{
?>
	<div class="row">
		<div class="col-md-6 col-md-offset-3 basic panel panel-default">
			<br>
			<h3>Lack of employee with the given id.</h3>
			<br>
			<br>
			<a class="btn btn-primary btn-sm" href="index.php"> Go to employees list </a>
			<br>
			<br>
			<br>
	</div>
<?php	
}
include_once('footer.php');
?>