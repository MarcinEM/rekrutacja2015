
/*
*	Marcin Michałek
*	main.js
* 	01.09.2015
*/

$('#addEmployeeForm').validate();
	
$(document).ready(function(){
	$('[data-toggle="popover"]').popover();	
	

    $( "#dateBirth" ).datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: '1940:2010'
    });
	$('#dateBirth').datepicker('option', 'dateFormat', 'yy-mm-dd');
    $( "#dateEmployment" ).datepicker({
		dateFormat: "yy-mm-dd",
		changeMonth: true,
		changeYear: true,
		yearRange: '1980:2030'
    });	
	$('#dateEmployment').datepicker('option', 'dateFormat', 'yy-mm-dd');
	
});