<?php
/*
*	Marcin Michałek
*	addNewEmployee.php
* 	01.09.2015
*/
include_once('login.php');
if(isset($_SESSION['login'])){
	if($_SESSION['login'] != 'yes'){
		die();
	}
}else{
	die();
}
include_once('startDb.php');
include_once('model/worker.php');
include_once('configuration.php');
include_once('header.php');

function massiveIssetCheck($string){
	$temps = explode(',', $string);
	foreach($temps as $temp){
		if(!isset($_POST[$temp])){
			return 0;
		}
	}
	return 1;
}

if(isset($_POST['flag'])){
	$flag = $_POST['flag'];
}else{
	$flag = false;
}

if($flag !== false){
	if(!massiveIssetCheck('bonus,salary,adress,phoneNumber,education,dateEmployment,workplace,dateBirth,lastname,firstname,flag')){
		$flag = false;
	}else{
		if(isset($_FILES['img']['tmp_name']) && isset($_FILES['img']['name'])){
			$newImgNamePart = hash('sha256', date('l jS F Y h:i:s A'));
			$newImgNamePart = substr($newImgNamePart, 0, 20);
			$fileNameArray = explode('.', $_FILES['img']['name']);
			$newImgName = $bigImgPath.$newImgNamePart.'.'.$fileNameArray[sizeof($fileNameArray)-1];	
			move_uploaded_file($_FILES['img']['tmp_name'], $newImgName);		

			$thumb = imagecreatetruecolor($smallImgX, $smallImgY);
			list($width, $height) = getimagesize($newImgName);	
			switch($fileNameArray[sizeof($fileNameArray)-1]){
				case 'jpg':	$source = imagecreatefromjpeg($newImgName);    // zrobić try catch
				break;
				case 'jpeg': $source = imagecreatefromjpeg($newImgName);
				break;
				case 'bmp': $source = imagecreatefrombmp($newImgName);
				break;
				case 'png': $source = imagecreatefrompng($newImgName);
				break;
				case 'gif': $source = imagecreatefromgif($newImgName);
				break;
				case 'gd': $source = imagecreatefromgd($newImgName);
				break;
				case 'gd2': $source = imagecreatefromgd2($newImgName);
				break;
				case 'webp': $source = imagecreatefromwebp($newImgName);
				break;
				case 'xbm': $source = imagecreatefromxbm($newImgName);
				break;
				case 'xpm': $source = imagecreatefromxpm($newImgName);
				break;
				default:
				break;
			}
			imagecopyresized($thumb, $source, 0, 0, 0, 0, $smallImgX, $smallImgY, $width, $height);
			imagejpeg($thumb, $smallImgPath.$newImgNamePart.'.jpeg');
				
		}else{
			$newImgName = '';
		}		
			
		$new = new Worker();
		$new->setFirstName($_POST['firstname']);
		$new->setLastName($_POST['lastname']);
		$new->setDayOfEmployment(new \DateTime($_POST['dateEmployment']));
		$new->setDayOfBirth(new \DateTime($_POST['dateBirth']));		
		$new->setWorkPlace($_POST['workplace']);
		$new->setEducation($_POST['education']);
		$new->setPhoneNumber($_POST['phoneNumber']);
		$new->setAdress($_POST['adress']);
		$new->setSalary($_POST['salary']);
		$new->setBonus($_POST['bonus']);		
		$new->setImgSrc($newImgNamePart); //sefsdff
		
		$entityManager->persist($new);
		$entityManager->flush();
		
		$flag = 'step one ok';
	}
}


if($flag === false){
?>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
   
<div class="row">
	<div class="col-md-6 col-md-offset-3 basic panel panel-default">
		<h3>Add new employee</h3>
		<br>
		<div id="validate"></div>		
		<form role="form" action="addNewEmployee.php" method="post" id="addEmployeeForm" enctype="multipart/form-data">		
			<div class="row">	
				<div class="col-md-10 col-md-offset-1">
					<table class="table">
						<tr>
							<td>
								Photo:
							</td>
							<td>
								<input type="file" name="img">
							</td>
						</tr>					
						<tr>
							<td>
								Firstname:
							</td>
							<td>
								<input type="text" name="firstname" minlength="3" autofocus required>
							</td>
						</tr>
						<tr>
							<td>
								Lastname:
							</td>
							<td>					
								<input type="text" name="lastname" minlength="3" required>
							</td>
						</tr>
						<tr>
							<td>
								Date of birth:
							</td>
							<td>	
								<input type="date" name="dateBirth" id="dateBirth" required>		
							</td>
						</tr>
						<tr>
							<td>
								Workplace:
							</td>
							<td>	
								<input type="text" name="workplace" minlength="3" required>
							</td>
						</tr>
						<tr>
							<td>
								Date of employment:
							</td>
							<td>	
								<input type="date" name="dateEmployment" id="dateEmployment" required>
							</td>
						</tr>
						<tr>
							<td>				
								Education:
							</td>
							<td>
								<textarea cols="18" rows="3" style="resize:none" name="education" minlength="3" required></textarea>	
							</td>
						</tr>
						<tr>
							<td>			
								phone number:
							</td>
							<td>
								<input type="digits" name="phoneNumber" id="phoneNumber" required>
							</td>
						</tr>
						<tr>
							<td>			
								Adress:
							</td>
							<td>
								<textarea cols="18" rows="1" style="resize:none" name="adress" minlength="3" required></textarea>
							</td>
						</tr>
						<tr>
							<td>						
								Salary:
							</td>
							<td>
								<input type="digits" name="salary" required >
							</td>
						</tr>
						<tr>
							<td>
								bonus:
							</td>
							<td>
								<input type="digits" name="bonus" required >	
							</td>
						</tr>						
					</table>
				</div>
			</div>	
			<input type="hidden" value="setData" name="flag">
			<a class="btn btn-warning btn-sm" href="index.php"> Return </a>
			<button class="btn btn-primary btn-sm" type="submit"> Add employee </button>	
			<br>
		</form>	
	</div>
</div>	


<?php
}elseif($flag == 'step one ok'){
?>

<div class="row">
	<div class="col-md-6 col-md-offset-3 basic panel panel-default">
		<h3>Add new employee</h3>
		<br>
		<div class="alert alert-success">New employee <?php echo $_POST['firstname'].' '.$_POST['lastname']; ?> was added.</div>
		<a class="btn btn-primary btn-sm" href="index.php"> Go to employees list </a>
		<br>
		<br>
		<br>
	</div>
</div>	
<?php	
}
include_once('footer.php');
?>




