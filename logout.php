<?php
/*
*	Marcin Michałek
*	logout.php
* 	01.09.2015
*/

$_SESSION = array();
session_regenerate_id(true);
session_start();
$_SESSION['login'] = 'no';
include_once('header.php');
include_once('loginForm.php');	
include_once('footer.php');

?>